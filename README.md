#### This project is for the Devops bootcamp exercise for

#### "Cloud Basics"

# NodeJS Project Listing App

## Overview
This NodeJS application, developed for the "Cloud & IaaS - DigitalOcean" module in the [TechWorld with Nana DevOps Bootcamp](https://www.techworld-with-nana.com/devops-bootcamp), allows team members and project managers to view ongoing projects online.

## Features
- Simple NodeJS application
- Online accessibility for collaboration
- Hosted on DigitalOcean

## Getting Started

### Prerequisites
- Git
- Node.js and npm
- DigitalOcean account

### Installation and Setup

#### Exercise 0: Clone Git Repository
1. Clone the repository:
git clone https://gitlab.com/twn-devops-bootcamp/latest/05-cloud/cloud-basics-exercises

2. Create your project repository from the clone.

#### Exercise 1: Package NodeJS App
1. Package the app into a tar file: bootcamp-node-project-1.0.0.tgz


#### Exercise 2: Create a DigitalOcean Server (Droplet)
1. Create a new droplet in DigitalOcean.

#### Exercise 3: Prepare Server for Node App
1. Install Node.js and npm on the droplet.

#### Exercise 4: Copy App and package.json
1. Transfer your Node.js app tar file and package.json to the droplet.

#### Exercise 5: Run Node App
1. Start the Node application in detached mode.

#### Exercise 6: Access from Browser - Configure Firewall
1. Open the required port on the droplet.
2. Access the app UI from a browser.

## Screenshots

### Screenshot 1: Project Repository
![cloud-basics-exercises](./images/project_repository.png)
- **Description**: A screenshot of the initial cloned repository in your Git interface, showcasing the repository structure.

### Screenshot 2: Packaged NodeJS App
![cloud-basics-exercises](./images/packaged_nodejs_app.png)
- **Description**: Display the tar file created from the npm pack command, showing the packaged application.

### Screenshot 3: DigitalOcean Droplet
![cloud-basics-exercises](./images/digitalocean_droplet.png)
- **Description**: A view of the DigitalOcean dashboard with your new droplet highlighted.

### Screenshot 4: Server Preparation
![cloud-basics-exercises](./images/server_preparation.png)
- **Description**: A terminal screenshot showing Node.js and npm installation commands and their successful execution.

### Screenshot 5: Application Deployment
![cloud-basics-exercises](./images/application_deployment.png)
- **Description**: Show the terminal with commands used to start the Node application, including any output.

### Screenshot 6: Running Application in Browser
![cloud-basics-exercises](./images/running_application_in_browser.png)
- **Description**: A browser window displaying the UI of the Node app, demonstrating successful access and functionality.

## Usage
Access the application via the server's IP address or domain name after deployment and port configuration.

## Contributing
Contributions, issues, and feature requests are welcome. For major changes, please open an issue first.

## License
MIT License

Copyright (c) [2023] [Mark Anthony Simon]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
